<?php

namespace stlswm\WeChatMp\UrlLink;

use stlswm\WeChatMp\CurlHttp;
use stlswm\WeChatMp\MiniProgram;
use stlswm\WeChatMp\Response;

class UrlLink
{
    use CurlHttp;

    public string $path;
    public string $query;
    public int    $expire_type;
    public int    $expire_time;
    public int    $expire_interval;
    /**
     * @var array 云开发静态网站自定义 H5 配置参数
     *            包含属性
     *            env               string
     *            domain            string
     *            path              string
     *            query             string
     *            resource_appid    string
     */
    public array  $cloud_base;
    public string $env_version;

    /**
     * @var MiniProgram
     */
    protected MiniProgram $miniProgram;

    public function __construct(MiniProgram $miniProgram)
    {
        $this->miniProgram = $miniProgram;
    }

    /**
     * @return Response
     */
    public function generate(): Response
    {
        $accessToken = $this->miniProgram->getAccessToken();
        $url = MiniProgram::$baseUri."/wxa/generate_urllink?access_token=$accessToken";
        $params = [
            'path',
            'query',
            'expire_type',
            'expire_time',
            'expire_interval',
            'cloud_base',
            'env_version',
        ];
        $body = [];
        foreach ($params as $param) {
            if (isset($this->$param)) {
                $body[$param] = $this->$param;
            }
        }
        return self::postJson($url, $body);
    }
}