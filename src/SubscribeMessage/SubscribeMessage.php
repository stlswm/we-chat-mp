<?php

namespace stlswm\WeChatMp\SubscribeMessage;

use stlswm\WeChatMp\CurlHttp;
use stlswm\WeChatMp\MiniProgram;
use stlswm\WeChatMp\Response;

/**
 * Class SubscribeMessage
 * @package stlswm\WeChatMp\SubscribeMessage
 */
class SubscribeMessage
{
    use CurlHttp;

    /**
     * @var MiniProgram
     */
    protected MiniProgram $miniProgram;

    public function __construct(MiniProgram $miniProgram)
    {
        $this->miniProgram = $miniProgram;
    }

    /**
     * @param  string       $accessToken
     * @param  string       $toUser
     * @param  string       $templateId
     * @param  string       $page
     * @param  array        $data
     * @param  string|null  $miniProgramState
     * @param  string|null  $lang
     * @return Response
     */
    public function send(
        string $accessToken,
        string $toUser,
        string $templateId,
        string $page,
        array $data,
        string $miniProgramState = null,
        string $lang = null
    ): Response {
        $url = MiniProgram::$baseUri."/cgi-bin/message/subscribe/send?access_token={$accessToken}";
        $req = [
            'touser'      => $toUser,
            'template_id' => $templateId,
            'page'        => $page,
            'data'        => $data,
        ];
        if ($miniProgramState) {
            $req['miniprogram_state'] = $miniProgramState;
        }
        if ($lang) {
            $req['lang'] = $lang;
        }
        return self::postJson($url, $req);
    }
}