<?php

namespace stlswm\WeChatMp;

class Response
{
    /**
     * @var bool $result
     */
    public bool $result = false;

    /**
     * @var string $message
     */
    public string $message = '';

    /**
     * @var string $errCode
     */
    public string $errCode = '';

    /**
     * @var string $errMsg
     */
    public string $errMsg = '';

    /**
     * @var mixed 返回数据
     */
    public $data = [];
}