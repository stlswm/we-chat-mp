<?php

namespace Safe;

use Config;
use PHPUnit\Framework\TestCase;
use stlswm\WeChatMp\MiniProgram;

include "../../vendor/autoload.php";
include '../Config.php';

class UrlLinkTest extends TestCase
{
    public function testGenerate()
    {
        $mp = MiniProgram::instance(Config::AppId, Config::Secret);
        $mp->setAccessToken(Config::AccessToken);
        $urlLinkGenerator = $mp->urlLinkGenerator();
        $urlLinkGenerator->path = 'pages/index/index';
        $urlLinkGenerator->expire_type = 1;
        $urlLinkGenerator->expire_interval = 30;
        $response = $urlLinkGenerator->generate();
        var_dump($response);
        $this->assertEquals(true, $response->result);
    }
}