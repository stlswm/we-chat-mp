<?php

use PHPUnit\Framework\TestCase;
use stlswm\WeChatMp\MiniProgram;

include "../vendor/autoload.php";
include './Config.php';

/**
 * Class SubscribeMessageTest
 */
class SubscribeMessageTest extends TestCase
{
    public function testSend()
    {
        $mp = MiniProgram::instance(Config::AppId, Config::Secret);
        $mp->setAccessToken(Config::AccessToken);
        $res = $mp->subscribeMessage()->send($mp->getAccessToken(), Config::OpenId,
            'WyF1DGRCMkPYxWhd1Yz5VyNwZy9X9uZ0eSf8xJDRPOA', '', [
                'thing2' => [
                    'value' => '测试',
                ],
                'thing1' => [
                    'value' => '通过',
                ],
                'thing3' => [
                    'value' => '备注',
                ]
            ]);
        var_dump($res);
    }
}